country_decisions = {
	start_dig_deeper = {
		major = yes
		potential = {
			capital_scope = {
				OR = {
					has_terrain = dwarven_hold
					has_terrain = dwarven_hold_surface
				}
				has_subterranean_race = yes
				NOT = { has_province_modifier = ruined_hold }
				NOT = { has_province_modifier = infested_hold }
				if = {
					limit = { OR = { province_id = 4119 province_id = 4311 } } #Big and Second capital
					NOT = { has_province_modifier = dig_11 }
				}
				else = {
					NOT = { has_province_modifier = dig_10 }
				}
				NOT = { has_province_modifier = dig_deeper }
				NOT = { has_province_modifier = dig_deeper_ai }
			}
			NOT = { has_country_flag = is_digging }
		}
		
		provinces_to_highlight = {
			is_capital = yes
			owned_by = ROOT
		}
		
		allow = {
			custom_trigger_tooltip = {
				tooltip = building_diggy_tooltip
				NOT = { has_country_flag = building_diggy }
			}
			OR = {
				has_country_modifier = dwarven_administration
				AND = {
					capital_scope = { has_subterranean_race = yes }
					adm_tech = 12
				}
				AND = {
					capital_scope = { has_integrated_dwarven_pop_trigger = yes }
					adm_tech = 7
				}
			}
			if = {
				limit = {
					capital_scope = { has_province_modifier = dig_1 }
				}
				capital_scope = {
					development = 40
				}
			}
			else_if = {	
				limit = {
					capital_scope = { has_province_modifier = dig_2 }
				}
				capital_scope = {
					development = 50
				}
			}
			else_if = {	
				limit = {
					capital_scope = { has_province_modifier = dig_3 }
				}
				capital_scope = {
					development = 60
				}
			}
			else_if = {	
				limit = {
					capital_scope = { has_province_modifier = dig_4 }
				}
				capital_scope = {
					development = 70
				}
			}
			else_if = {	
				limit = {
					capital_scope = { has_province_modifier = dig_5 }
				}
				capital_scope = {
					development = 80
				}
			}
			else_if = {	
				limit = {
					capital_scope = { has_province_modifier = dig_6 }
				}
				capital_scope = {
					development = 90
				}
			}
			else_if = {	
				limit = {
					capital_scope = { has_province_modifier = dig_7 }
				}
				capital_scope = {
					development = 100
				}
			}
			else_if = {	
				limit = {
					capital_scope = { has_province_modifier = dig_8 }
				}
				capital_scope = {
					development = 110
				}
			}
			else_if = {
				limit = {
					capital_scope = { has_province_modifier = dig_9 }
				}
				capital_scope = {
					development = 120
				}
			}
		}
		effect = {
			if = {
				limit = { ai = no }
				add_years_of_income = -1
				capital_scope = {
					add_province_modifier = {
						name = dig_deeper
						duration = -1
					}
					hidden_effect = { set_province_flag = dig_capital }
				}
			}
			else = {
				capital_scope = {
					add_province_modifier = {
						name = dig_deeper_ai
						duration = -1
					}
					hidden_effect = { set_province_flag = dig_capital }
				}
			}
			hidden_effect = { set_country_flag = is_digging }
		}
		ai_will_do = {
			factor = 400
		}
	}
	
	start_hold_restoration = {
		major = yes
		potential = {
			any_owned_province = {
				OR = {
					has_province_modifier = ruined_hold
					has_province_modifier = partially_ruined_hold
					has_province_modifier = damaged_hold
				}
				NOT = { has_province_modifier = hold_being_restored }
				is_city = yes
			}
		}
		
		provinces_to_highlight = {
			OR = {
				has_province_modifier = ruined_hold
				has_province_modifier = partially_ruined_hold
				has_province_modifier = damaged_hold
			}
			owned_by = ROOT
		}
		
		allow = {
			NOT = { num_of_rebel_armies = 1 }
			if = {
				limit = {
					any_owned_province = {
						OR = {
							has_province_modifier = ruined_hold
							has_province_modifier = partially_ruined_hold
							has_province_modifier = damaged_hold
						}
						NOT = { has_province_modifier = hold_being_restored }
						development = 3
						NOT = { development = 30 }
						is_city = yes
					}
					OR = {
						NOT = { num_of_cities = 2 }
						ai = yes
					}
				}
				adm_power = 100
				mil_power = 100
				dip_power = 100
				any_owned_province = {
					OR = {
						has_province_modifier = ruined_hold
						has_province_modifier = partially_ruined_hold
						has_province_modifier = damaged_hold
					}
					NOT = { has_province_modifier = hold_being_restored }
					development = 3
					is_city = yes
				}
			}
			else_if = {
				limit = {
					any_owned_province = {
						OR = {
							has_province_modifier = ruined_hold
							has_province_modifier = partially_ruined_hold
							has_province_modifier = damaged_hold
						}
						NOT = { has_province_modifier = hold_being_restored }
						development = 3
						NOT = { development = 30 }
						is_city = yes
					}
				}
				adm_power = 50
				mil_power = 50
				dip_power = 50
				treasury = 250
				any_owned_province = {
					OR = {
						has_province_modifier = ruined_hold
						has_province_modifier = partially_ruined_hold
						has_province_modifier = damaged_hold
					}
					NOT = { has_province_modifier = hold_being_restored }
					development = 3
					NOT = { development = 30 }
					is_city = yes
				}
			}
			else_if = {
				limit = {
					any_owned_province = {
						OR = {
							has_province_modifier = ruined_hold
							has_province_modifier = partially_ruined_hold
							has_province_modifier = damaged_hold
						}
						NOT = { has_province_modifier = hold_being_restored }
						development = 30
						NOT = { development = 60 }
						is_city = yes
					}
				}
				adm_power = 150
				mil_power = 150
				dip_power = 150
				treasury = 750
				any_owned_province = {
					OR = {
						has_province_modifier = ruined_hold
						has_province_modifier = partially_ruined_hold
						has_province_modifier = damaged_hold
					}
					NOT = { has_province_modifier = hold_being_restored }
					development = 30
					NOT = { development = 60 }
					is_city = yes
				}
			}
			else_if = {
				limit = {
					any_owned_province = {
						OR = {
							has_province_modifier = ruined_hold
							has_province_modifier = partially_ruined_hold
							has_province_modifier = damaged_hold
						}
						NOT = { has_province_modifier = hold_being_restored }
						development = 60
						NOT = { development = 90 }
						is_city = yes
					}
				}
				adm_power = 300
				mil_power = 300
				dip_power = 300
				treasury = 1500
				any_owned_province = {
					OR = {
						has_province_modifier = ruined_hold
						has_province_modifier = partially_ruined_hold
						has_province_modifier = damaged_hold
					}
					NOT = { has_province_modifier = hold_being_restored }
					development = 60
					NOT = { development = 90 }
					is_city = yes
				}
			}
			else_if = {
				limit = {
					any_owned_province = {
						OR = {
							has_province_modifier = ruined_hold
							has_province_modifier = partially_ruined_hold
							has_province_modifier = damaged_hold
						}
						NOT = { has_province_modifier = hold_being_restored }
						development = 90
						is_city = yes
					}
				}
				adm_power = 500
				mil_power = 500
				dip_power = 500
				treasury = 3000
				any_owned_province = {
					OR = {
						has_province_modifier = ruined_hold
						has_province_modifier = partially_ruined_hold
						has_province_modifier = damaged_hold
					}
					NOT = { has_province_modifier = hold_being_restored }
					development = 90
					is_city = yes
				}
			}
		}
		
		effect = {
			if = {
				limit = {
					any_owned_province = {
						OR = {
							has_province_modifier = ruined_hold
							has_province_modifier = partially_ruined_hold
							has_province_modifier = damaged_hold
						}
						NOT = { has_province_modifier = hold_being_restored }
						development = 3
						is_city = yes
					}
					OR = {
						NOT = { num_of_cities = 2 }
						ai = yes
					}
				}
				add_adm_power = -100
				add_mil_power = -100
				add_dip_power = -100
				random_owned_province = {
					limit = {
						development = 3
						OR = {
							has_province_modifier = ruined_hold
							has_province_modifier = partially_ruined_hold
							has_province_modifier = damaged_hold
						}
						NOT = { has_province_modifier = hold_being_restored }
						is_city = yes
					}
					add_province_modifier = {
						name = hold_being_restored
						duration = -1
					}
					hidden_effect = { set_province_flag = being_restored }
				}
			}
			else_if = {
				limit = {
					any_owned_province = {
						OR = {
							has_province_modifier = ruined_hold
							has_province_modifier = partially_ruined_hold
							has_province_modifier = damaged_hold
						}
						NOT = { has_province_modifier = hold_being_restored }
						development = 3
						NOT = { development = 30 }
						is_city = yes
					}
				}
				add_adm_power = -50
				add_mil_power = -50
				add_dip_power = -50
				add_treasury = -250
				random_owned_province = {
					limit = {
						development = 3
						NOT = { development = 30 }
						OR = {
							has_province_modifier = ruined_hold
							has_province_modifier = partially_ruined_hold
							has_province_modifier = damaged_hold
						}
						NOT = { has_province_modifier = hold_being_restored }
						is_city = yes
					}
					add_province_modifier = {
						name = hold_being_restored
						duration = -1
					}
					hidden_effect = { set_province_flag = being_restored }
				}
			}
			else_if = {
				limit = {
					any_owned_province = {
						OR = {
							has_province_modifier = ruined_hold
							has_province_modifier = partially_ruined_hold
							has_province_modifier = damaged_hold
						}
						NOT = { has_province_modifier = hold_being_restored }
						development = 30
						NOT = { development = 60 }
						is_city = yes
					}
				}
				add_adm_power = -150
				add_mil_power = -150
				add_dip_power = -150
				add_treasury = -750
				random_owned_province = {
					limit = {
						development = 30
						NOT = { development = 60 }
						OR = {
							has_province_modifier = ruined_hold
							has_province_modifier = partially_ruined_hold
							has_province_modifier = damaged_hold
						}
						NOT = { has_province_modifier = hold_being_restored }
						is_city = yes
					}
					add_province_modifier = {
						name = hold_being_restored
						duration = -1
					}
					hidden_effect = { set_province_flag = being_restored }
				}
			}
			else_if = {
				limit = {
					any_owned_province = {
						OR = {
							has_province_modifier = ruined_hold
							has_province_modifier = partially_ruined_hold
							has_province_modifier = damaged_hold
						}
						NOT = { has_province_modifier = hold_being_restored }
						development = 60
						NOT = { development = 90 }
						is_city = yes
					}
					hidden_effect = { set_province_flag = being_restored }
				}
				add_adm_power = -300
				add_mil_power = -300
				add_dip_power = -300
				add_treasury = -1500
				random_owned_province = {
					limit = {
						development = 60
						NOT = { development = 90 }
						OR = {
							has_province_modifier = ruined_hold
							has_province_modifier = partially_ruined_hold
							has_province_modifier = damaged_hold
						}
						NOT = { has_province_modifier = hold_being_restored }
						is_city = yes
					}
					add_province_modifier = {
						name = hold_being_restored
						duration = -1
					}
					hidden_effect = { set_province_flag = being_restored }
				}
			}
			else_if = {
				limit = {
					any_owned_province = {
						OR = {
							has_province_modifier = ruined_hold
							has_province_modifier = partially_ruined_hold
							has_province_modifier = damaged_hold
						}
						NOT = { has_province_modifier = hold_being_restored }
						development = 90
						is_city = yes
					}
				}
				add_adm_power = -500
				add_mil_power = -500
				add_dip_power = -500
				add_treasury = -3000
				random_owned_province = {
					limit = {
						development = 90
						OR = {
							has_province_modifier = ruined_hold
							has_province_modifier = partially_ruined_hold
							has_province_modifier = damaged_hold
						}
						NOT = { has_province_modifier = hold_being_restored }
						is_city = yes
					}
					add_province_modifier = {
						name = hold_being_restored
						duration = -1
					}
					hidden_effect = { set_province_flag = being_restored }
				}
			}
			hidden_effect = { set_country_flag = restoring_hold }
		}
		ai_will_do = {
			factor = 400
			
		}
	}
	
	clear_infested_hold = {
		major = yes
		potential = {
			any_owned_province = {
				has_province_modifier = infested_hold
			}
		}
		
		provinces_to_highlight = {
			has_province_modifier = infested_hold
			owned_by = ROOT
		}
		
		allow = {
			NOT = {
				has_spawned_rebels = cave_goblin_rebel
				has_spawned_rebels = cave_orc_rebel
				is_at_war = yes
			}
		}
		
		effect = {
			random_owned_province  = {
				limit = {
					has_province_modifier = infested_hold
				}
				if = {
					limit = { development = 3 }
					spawn_rebels = {
						type = cave_orc_rebel
						culture = black_orc
						size = 1
					}
				}
				if = {
					limit = { development = 20 }
					spawn_rebels = {
						type = cave_orc_rebel
						culture = black_orc
						size = 1
					}
				}
				if = {
					limit = { development = 35 }
					spawn_rebels = {
						type = cave_orc_rebel
						culture = black_orc
						size = 1
					}
				}
				remove_province_modifier = infested_hold
				add_permanent_province_modifier = {
					name = ruined_hold
					duration = -1
				}
			}
		}
		ai_will_do = {
			factor = 400
			
			modifier = {
				factor = 0
				OR = {
					NOT = { army_size = 12 }
					is_at_war = yes
				}
			}
			
			modifier = {
				factor = 0
				
				any_owned_province = {
					has_province_modifier = infested_hold
					development = 20
				}
				NOT = { army_size = 20 }
			}
		}
	}
	
	build_magma_forge = {
		major = yes
		potential = {
			capital_scope = {
				has_province_modifier = hold_foundry
			}
			NOT = { 
				has_country_flag = building_diggy
				has_country_flag = building_magma_forge
			}
		}
		
		provinces_to_highlight = {
			is_capital = yes
			owned_by = ROOT
		}
		
		allow = {
			capital_scope = {
				has_province_modifier = hold_foundry
				development = 90
			}
			adm_power = 100
			dip_power = 100
			treasury = 3000
			custom_trigger_tooltip = {
				tooltip = dont_dig_tooltip
				NOT = { has_country_flag = is_digging }
			}
		}
		
		effect = {
			add_treasury = -3000
			add_adm_power = -100
			add_dip_power = -100
			hidden_effect = {
				set_country_flag = building_diggy
				set_country_flag = building_magma_forge
				country_event = { id = diggy_project.1 }
			}
			capital_scope = {
				add_province_modifier = {
					name = developing_advanced_infrastructure
					duration = -1
				}
			}
		}
		
		ai_will_do = {
			factor = 400
		}
	}
	
	build_artificier_hall = {
		major = yes
		potential = {
			capital_scope = {
				has_province_modifier = hold_city
			}
			NOT = { 
				has_country_flag = building_diggy
				has_country_flag = building_artificier_hall
			}
		}
		
		provinces_to_highlight = {
			is_capital = yes
			owned_by = ROOT
		}
		
		allow = {
			capital_scope = {
				has_province_modifier = hold_city
				development = 90
			}
			adm_power = 100
			dip_power = 100
			treasury = 3000
			custom_trigger_tooltip = {
				tooltip = dont_dig_tooltip
				NOT = { has_country_flag = is_digging }
			}
		}
		
		effect = {
			add_treasury = -3000
			add_adm_power = -100
			add_dip_power = -100
			hidden_effect = {
				set_country_flag = building_diggy
				set_country_flag = building_artificier_hall
				country_event = { id = diggy_project.3 }
			}
			capital_scope = {
				add_province_modifier = {
					name = developing_advanced_infrastructure
					duration = -1
				}
			}
		}
		
		ai_will_do = {
			factor = 400
		}
	}
	
	build_engineer_manufactory = {
		major = yes
		potential = {
			capital_scope = {
				has_province_modifier = hold_artisan
			}
			NOT = { 
				has_country_flag = building_diggy
				has_country_flag = building_engineer_manufactory
			}
		}
		
		provinces_to_highlight = {
			is_capital = yes
			owned_by = ROOT
		}
		
		allow = {
			capital_scope = {
				has_province_modifier = hold_artisan
				development = 90
			}
			adm_power = 100
			dip_power = 100
			treasury = 3000
			custom_trigger_tooltip = {
				tooltip = dont_dig_tooltip
				NOT = { has_country_flag = is_digging }
			}
		}
		
		effect = {
			add_treasury = -3000
			add_adm_power = -100
			add_dip_power = -100
			hidden_effect = {
				set_country_flag = building_diggy
				set_country_flag = building_engineer_manufactory
				country_event = { id = diggy_project.5 }
			}
			capital_scope = {
				add_province_modifier = {
					name = developing_advanced_infrastructure
					duration = -1
				}
			}
		}
		
		ai_will_do = {
			factor = 400
		}
	}
	
	build_high_gardens = {
		major = yes
		potential = {
			capital_scope = {
				has_province_modifier = hold_farm
			}
			NOT = { 
				has_country_flag = building_diggy
				has_country_flag = building_high_gardens
			}
		}
		
		provinces_to_highlight = {
			is_capital = yes
			owned_by = ROOT
		}
		
		allow = {
			capital_scope = {
				has_province_modifier = hold_farm
				development = 90
			}
			adm_power = 100
			dip_power = 100
			treasury = 3000
			custom_trigger_tooltip = {
				tooltip = dont_dig_tooltip
				NOT = { has_country_flag = is_digging }
			}
		}
		
		effect = {
			add_treasury = -3000
			add_adm_power = -100
			add_dip_power = -100
			hidden_effect = {
				set_country_flag = building_diggy
				set_country_flag = building_high_gardens
				country_event = { id = diggy_project.7 }
			}
			capital_scope = {
				add_province_modifier = {
					name = developing_advanced_infrastructure
					duration = -1
				}
			}
		}
		
		ai_will_do = {
			factor = 400
		}
	}
	
	build_military_academy = {
		major = yes
		potential = {
			capital_scope = {
				has_province_modifier = hold_military
			}
			NOT = { 
				has_country_flag = building_diggy
				has_country_flag = building_military_academy
			}
		}
		
		provinces_to_highlight = {
			is_capital = yes
			owned_by = ROOT
		}
		
		allow = {
			capital_scope = {
				has_province_modifier = hold_military
				development = 90
			}
			adm_power = 100
			dip_power = 100
			treasury = 3000
			custom_trigger_tooltip = {
				tooltip = dont_dig_tooltip
				NOT = { has_country_flag = is_digging }
			}
		}
		
		effect = {
			add_treasury = -3000
			add_adm_power = -100
			add_dip_power = -100
			hidden_effect = {
				set_country_flag = building_diggy
				set_country_flag = building_military_academy
				country_event = { id = diggy_project.9 }
			}
			capital_scope = {
				add_province_modifier = {
					name = developing_advanced_infrastructure
					duration = -1
				}
			}
		}
		
		ai_will_do = {
			factor = 400
		}
	}
	
	explore_special_cavern = {
		major = yes
		potential = {
			any_owned_province = {
				has_province_modifier = special_cave
				NOT = { has_province_modifier = exploring_cave }
			}
		}
		
		provinces_to_highlight = {
			has_province_modifier = special_cave
			NOT = { has_province_modifier = exploring_cave }
			owned_by = ROOT
		}
		
		allow = {
			any_owned_province = {
				has_province_modifier = special_cave
				NOT = { has_province_modifier = exploring_cave }
				is_city = yes
			}
			dip_power = 20
			treasury = 50
		}
		
		effect = {
			add_treasury = -50
			add_dip_power = -20
			random_owned_province = {
				limit = {
					has_province_modifier = special_cave
					NOT = { has_province_modifier = exploring_cave }
					is_city = yes
				}
				add_province_modifier = {
					name = exploring_cave
					duration = -1
				}
			}
		}
		
		ai_will_do = {
			factor = 400
		}
	}
	
	rebuild_road = {
		major = yes
		potential = {
			any_owned_province = {
				has_province_modifier = dwarovar_rail
				NOT = { has_province_modifier = restoring_rail }
			}
			adm_tech = 12
		}
		
		provinces_to_highlight = {
			has_province_modifier = dwarovar_rail
			NOT = { has_province_modifier = restoring_rail }
			any_neighbor_province = {
				OR = {
					has_province_modifier = restored_rail
					has_province_modifier = advanced_rail
					AND = {
						OR = {
							has_terrain = dwarven_hold
							has_terrain = dwarven_hold_surface
						}
						NOT = { has_province_modifier = ruined_hold }
						NOT = { has_province_modifier = infested_hold }
					}
				}
				owned_by = ROOT
			}
			owned_by = ROOT
		}
		
		allow = {
			adm_tech = 16
			any_owned_province = {
				has_province_modifier = dwarovar_rail
				NOT = { has_province_modifier = restoring_rail }
				is_city = yes
				any_neighbor_province = {
					OR = {
						has_province_modifier = restored_rail
						has_province_modifier = advanced_rail
						AND = {
							OR = {
								has_terrain = dwarven_hold
								has_terrain = dwarven_hold_surface
							}
							NOT = { has_province_modifier = ruined_hold }
							NOT = { has_province_modifier = infested_hold }
						}
					}
					owned_by = ROOT
				}
			}
			custom_trigger_tooltip = {
				tooltip = recent_rail_construction_tooltip
				OR = {
					NOT = { has_country_flag = dworf_rail_flag }
					had_country_flag = {
						flag = dworf_rail_flag
						days = 30
					}
				}
			}
		}
		
		effect = {
			random_owned_province = {
				limit = {
					has_province_modifier = dwarovar_rail
					NOT = { has_province_modifier = restoring_rail }
					is_city = yes
					any_neighbor_province = {
						OR = {
							has_province_modifier = restored_rail
							has_province_modifier = advanced_rail
							AND = {
								OR = {
									has_terrain = dwarven_hold
									has_terrain = dwarven_hold_surface
								}
								NOT = { has_province_modifier = ruined_hold }
								NOT = { has_province_modifier = infested_hold }
							}
						}
						owned_by = ROOT
					}
				}
				add_province_modifier = {
					name = restoring_rail
					duration = -1
				}
				hidden_effect = {
					province_event = { id = diggy.36 days = 720 random = 365 }
				}
			}
			hidden_effect = { set_country_flag = dworf_rail_flag }
		}
		
		ai_will_do = {
			factor = 400
		}
	}
	
	upgrade_road = {
		major = yes
		potential = {
			any_owned_province = {
				has_province_modifier = restored_rail
				NOT = { has_province_modifier = upgrading_rail }
			}
			adm_tech = 18
		}
		
		provinces_to_highlight = {
			has_province_modifier = restored_rail
			NOT = { has_province_modifier = upgrading_rail }
			any_neighbor_province = {
				OR = {
					has_province_modifier = advanced_rail
					AND = {
						OR = {
							has_terrain = dwarven_hold
							has_terrain = dwarven_hold_surface
						}
						NOT = { has_province_modifier = ruined_hold }
						NOT = { has_province_modifier = infested_hold }
					}
				}
				owned_by = ROOT
			}
			owned_by = ROOT
		}
		
		allow = {
			adm_tech = 21
			any_owned_province = {
				has_province_modifier = restored_rail
				NOT = { has_province_modifier = upgrading_rail }
				is_city = yes
				any_neighbor_province = {
					OR = {
						has_province_modifier = advanced_rail
						AND = {
							OR = {
								has_terrain = dwarven_hold
								has_terrain = dwarven_hold_surface
							}
							NOT = { has_province_modifier = ruined_hold }
							NOT = { has_province_modifier = infested_hold }
						}
					}
					owned_by = ROOT
				}
			}
			custom_trigger_tooltip = {
				tooltip = recent_rail_construction_tooltip
				OR = {
					NOT = { has_country_flag = dworf_rail_flag }
					had_country_flag = {
						flag = dworf_rail_flag
						days = 30
					}
				}
			}
		}
		
		effect = {
			random_owned_province = {
				limit = {
					has_province_modifier = restored_rail
					NOT = { has_province_modifier = upgrading_rail }
					is_city = yes
					any_neighbor_province = {
						OR = {
							has_province_modifier = advanced_rail
							AND = {
								OR = {
									has_terrain = dwarven_hold
									has_terrain = dwarven_hold_surface
								}
								NOT = { has_province_modifier = ruined_hold }
								NOT = { has_province_modifier = infested_hold }
							}
						}
						owned_by = ROOT
					}
				}
				if = {
					limit = { ai = no }
					add_treasury = -25
				}
				add_province_modifier = {
					name = upgrading_rail
					duration = -1
				}
				hidden_effect = {
					province_event = { id = diggy.42 days = 720 random = 365 }
				}
			}
			hidden_effect = { set_country_flag = dworf_rail_flag }
		}
		
		ai_will_do = {
			factor = 400
		}
	}
	
	destroy_old_gate = {
		major = yes
		potential = {
			has_disaster = goblin_tide
			any_owned_province = { has_province_modifier = old_gate }
		}
		
		provinces_to_highlight = {
			has_province_modifier = old_gate
			owned_by = ROOT
		}
		
		allow = {
			any_owned_province = {
				has_province_modifier = old_gate
				owned_by = ROOT
				controlled_by = ROOT
				num_of_units_in_province = {
					who = ROOT
					amount = 10
				}
			}
		}
		
		effect = {
			random_owned_province = {
				limit = {
					has_province_modifier = old_gate
					owned_by = ROOT
					controlled_by = ROOT
					num_of_units_in_province = {
						who = ROOT
						amount = 10
					}
				}
				remove_province_modifier = old_gate
				spawn_rebels = {
					type = goblin_tide_rebel
					culture = cave_goblin
					size = 3
				}
			}
		}
		
		ai_will_do = {
			factor = 400
		}
	}
	
	build_orc_barrack = {
		major = yes
		potential = {
			primary_culture = black_orc
			any_owned_province = {
				OR = {
					has_terrain = dwarven_hold
					has_terrain = dwarven_hold_surface
				}
				NOT = { has_province_modifier = orc_hold_barrack }
				NOT = { has_province_modifier = black_orc_building_1 }
				is_city = yes
				culture = black_orc
			}
		}
		
		provinces_to_highlight = {
			OR = {
				has_terrain = dwarven_hold
				has_terrain = dwarven_hold_surface
			}
			NOT = { has_province_modifier = orc_hold_barrack }
			NOT = { has_province_modifier = black_orc_building_1 }
			is_city = yes
			culture = black_orc
			
			owned_by = ROOT
		}
		
		allow = {
			any_owned_province = {
				OR = {
					has_terrain = dwarven_hold
					has_terrain = dwarven_hold_surface
				}
				NOT = { has_province_modifier = orc_hold_barrack }
				NOT = { has_province_modifier = black_orc_building_1 }
				NOT = { has_province_modifier = infested_hold }
				is_city = yes
				culture = black_orc
			}
			mil_power = 300
		}
		
		effect = {
			add_mil_power = -300
			random_owned_province = {
				limit = {
					OR = {
						has_terrain = dwarven_hold
						has_terrain = dwarven_hold_surface
					}
					NOT = { has_province_modifier = orc_hold_barrack }
					NOT = { has_province_modifier = black_orc_building_1 }
					is_city = yes
					culture = black_orc
				}
				add_province_modifier = {
					name = black_orc_building_1
					duration = -1
				}
				hidden_effect = { province_event = { id = diggy.112 days = 365 random = 100 } }
			}
		}
		
		ai_will_do = {
			factor = 400
		}
	}
	
	build_orc_raider = {
		major = yes
		potential = {
			primary_culture = black_orc
			any_owned_province = {
				OR = {
					has_terrain = dwarven_hold
					has_terrain = dwarven_hold_surface
				}
				NOT = { has_province_modifier = orc_hold_raider }
				NOT = { has_province_modifier = black_orc_building_2 }
				is_city = yes
				culture = black_orc
			}
		}
		
		provinces_to_highlight = {
			OR = {
				has_terrain = dwarven_hold
				has_terrain = dwarven_hold_surface
			}
			NOT = { has_province_modifier = orc_hold_raider }
			NOT = { has_province_modifier = black_orc_building_2 }
			is_city = yes
			culture = black_orc
			
			owned_by = ROOT
		}
		
		allow = {
			any_owned_province = {
				OR = {
					has_terrain = dwarven_hold
					has_terrain = dwarven_hold_surface
				}
				NOT = { has_province_modifier = orc_hold_raider }
				NOT = { has_province_modifier = black_orc_building_2 }
				NOT = { has_province_modifier = infested_hold }
				is_city = yes
				culture = black_orc
			}
			dip_power = 300
		}
		
		effect = {
			add_dip_power = -300
			random_owned_province = {
				limit = {
					OR = {
						has_terrain = dwarven_hold
						has_terrain = dwarven_hold_surface
					}
					NOT = { has_province_modifier = orc_hold_raider }
					NOT = { has_province_modifier = black_orc_building_2 }
					is_city = yes
					culture = black_orc
				}
				add_province_modifier = {
					name = black_orc_building_2
					duration = -1
				}
				hidden_effect = { province_event = { id = diggy.112 days = 365 random = 100 } }
			}
		}
		
		ai_will_do = {
			factor = 400
		}
	}
	
	build_orc_arena = {
		major = yes
		potential = {
			primary_culture = black_orc
			any_owned_province = {
				OR = {
					has_terrain = dwarven_hold
					has_terrain = dwarven_hold_surface
				}
				NOT = { has_province_modifier = orc_hold_arena }
				NOT = { has_province_modifier = black_orc_building_3 }
				is_city = yes
				culture = black_orc
			}
		}
		
		provinces_to_highlight = {
			OR = {
				has_terrain = dwarven_hold
				has_terrain = dwarven_hold_surface
			}
			NOT = { has_province_modifier = orc_hold_arena }
			NOT = { has_province_modifier = black_orc_building_3 }
			is_city = yes
			culture = black_orc
			
			owned_by = ROOT
		}
		
		allow = {
			any_owned_province = {
				OR = {
					has_terrain = dwarven_hold
					has_terrain = dwarven_hold_surface
				}
				NOT = { has_province_modifier = orc_hold_arena }
				NOT = { has_province_modifier = black_orc_building_3 }
				NOT = { has_province_modifier = infested_hold }
				is_city = yes
				culture = black_orc
			}
			adm_power = 300
		}
		
		effect = {
			add_adm_power = -300
			random_owned_province = {
				limit = {
					OR = {
						has_terrain = dwarven_hold
						has_terrain = dwarven_hold_surface
					}
					NOT = { has_province_modifier = orc_hold_arena }
					NOT = { has_province_modifier = black_orc_building_3 }
					is_city = yes
					culture = black_orc
				}
				add_province_modifier = {
					name = black_orc_building_3
					duration = -1
				}
				hidden_effect = { province_event = { id = diggy.112 days = 365 random = 100 } }
			}
		}
		
		ai_will_do = {
			factor = 400
		}
	}
	
	purge_warband = {
		major = yes
		potential = {
			NOT = { num_of_cities = 3 }
			culture_group = dwarven
			capital_scope = {
				continent = serpentspine
			}
			any_owned_province = {
				is_capital = no
				OR = {
					culture = black_orc
					culture = cave_goblin
				}
				is_city = yes
			}
		}
		
		provinces_to_highlight = {
			is_capital = no
			OR = {
				culture = black_orc
				culture = cave_goblin
			}
			is_city = yes
			
			owned_by = ROOT
		}
		
		allow = {
			any_owned_province = {
				is_capital = no
				OR = {
					culture = black_orc
					culture = cave_goblin
				}
				is_city = yes
			}
		}
		
		effect = {
			add_prestige = 35
			add_mil_power = 75
			add_adm_power = 75
			add_dip_power = 75
			random_owned_province = {
				limit = {
					is_capital = no
					OR = {
						culture = black_orc
						culture = cave_goblin
					}
					is_city = yes
				}
				custom_tooltip = purge_warband_tooltip
				hidden_effect = { 
					cede_province = XXX
					multiply_colonysize = 0
				}
			}
		}
		
		ai_will_do = {
			factor = 400
		}
	}
	
	restore_irrigation = {
		major = yes
		potential = {
			NOT = { has_global_flag = serpent_vale_irrigation }
			areadw01_area = {
				type = all
				owned_by = ROOT
			}
			NOT = { has_country_flag = repairing_irrigation }
			NOT = {
				AND = {
					has_global_flag = irrigation_east_done
					has_global_flag = irrigation_north_done
					has_global_flag = irrigation_south_done
					has_global_flag = irrigation_west_done
				}
			}
		}
		
		provinces_to_highlight = {
			has_province_modifier = flooded_province
			owned_by = ROOT
		}
		
		allow = {
			areadw01_area = {
				type = all
				is_city = yes
			}
			if = {
				limit = { ai = no }
				treasury = 1500
			}
			else = {
				treasury = 500
			}
			adm_tech = 13
			adm_power = 200
		}
		
		effect = {
			hidden_effect = { set_country_flag = repairing_irrigation }
			country_event = { id = diggy.43 }
			#custom_tooltip = restore_irrigation_tooltip
		}
		
		ai_will_do = {
			factor = 400
		}
	}
	
	the_great_dam = {
		major = yes
		potential = {
			has_country_modifier = dwarven_administration
			NOT = { has_global_flag = harpylen_dam_build }
			capital_scope = { continent = serpentspine }
			adm_tech = 14
			NOT = { has_country_flag = dam_menu }
			NOT = { has_country_flag = constructing_dam }
		}
		
		provinces_to_highlight = {
			province_id = 676
		}
		
		allow = {
			owns = 676
			adm_tech = 16
			adm_power = 100
			treasury = 12500
		}
		
		effect = {
			hidden_effect = { set_country_flag = dam_menu }
			country_event = { id = diggy_project.11 }
		}
		
		ai_will_do = {
			factor = 400
		}
	}
}
