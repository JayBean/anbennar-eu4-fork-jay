


culture = black_orc
religion = great_dookan
trade_goods = unknown
hre = no
base_tax = 12
base_production = 15
base_manpower = 11
native_size = 0
native_ferocity = 10
native_hostileness = 10
center_of_trade = 1

add_permanent_province_modifier = {
	name = infested_hold
	duration = -1
}