country_decisions = {

	abandon_our_piracy = {
		major = yes
		potential = {
			tag = A74
			NOT = { has_country_flag = abandoned_piracy }
		}

		allow = {
			adm_tech = 5
		}
		
		effect = {
			add_government_reform = oligarchy_reform
			every_country = {
				limit = {
				is_emperor = yes
				}
				add_opinion = {
				who = A74
				modifier = pirate_republic_disbanded
				}
			}
				
			add_country_modifier = {
				name = pirate_legacy
				duration = -1
			}
			set_country_flag = abandoned_piracy
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	
	
	abolish_slavery_in_nathalaire = {
		major = yes
		potential = {
			tag = A74
			NOT = { has_country_flag = abolished_slavery }
		}
		allow = {
			dip_tech = 6
		}
		effect = {
			451 = { change_trade_goods = gems }
			set_country_flag = abolished_slavery
		}
		ai_will_do = {
			factor = 1
		}
	}
}