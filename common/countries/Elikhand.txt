#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 232  118  121 }

revolutionary_colors = { 232  118  121 }

historical_idea_groups = {
	expansion_ideas
	exploration_ideas
	quantity_ideas
	diplomatic_ideas	
	offensive_ideas
	defensive_ideas
	economic_ideas	
	innovativeness_ideas
}

historical_units = {
	western_medieval_infantry
	chevauchee
	western_men_at_arms
	swiss_landsknechten
	dutch_maurician
	french_caracolle
	anglofrench_line
	french_dragoon
	french_bluecoat
	french_cuirassier
	french_impulse
	mixed_order_infantry
	open_order_cavalry
	napoleonic_square
	napoleonic_lancers
}

monarch_names = {


	#Generic Kheteratan Name List
	"Bayek #0" = 1
	"Semut #0" = 1
	"Nuhem #0" = 1
	"Siout #0" = 1
	"Murdas #0" = 1
	"Seker #0" = 1
	"Benat #0" = 1
	"Aahmes #0" = 1
	"Sefu #0" = 1
	"Amer #0" = 1
	"Tariush #0" = 1
	"Babar #0" = 1
	"Tumain #0" = 1
	"Shishak #0" = 1
	"Res #0" = 1
	"Chener #0" = 1
	"Akir #0" = 1
	"Akil #0" = 1
	"Sab #0" = 1
	"Saban #0" = 1
	"Khair #0" = 1
	"Kair #0" = 1
	"Kenmu #0" = 1
	"Aakh #0" = 1
	"Jabar #0" = 1
	"Nazem #0" = 1
	"Entef #0" = 1
	"Nail #0" = 1
	"Chufar #0" = 1
	"Raad #0" = 1
	"Hekeb #0" = 1
	"Bhenak #0" = 1
	"Bonep #0" = 1
	"Zalik #0" = 1
	
	"Jenda #0" = -10
	"Jendayi #0" = -10
	"Thema #0" = -10
	"Antarta #0" = -10
	"Bakrana #0" = -10
	"Sofh #0" = -10
	"Naunet #0" = -10
	"Amina #0" = -10
	"Madsa #0" = -10
	"Nadia #0" = -10
	"Anqa #0" = -10
	"Zalika #0" = -10
	"Samia #0" = -10
	"Esi #0" = -10
	"Esia #0" = -10
	"Nane #0" = -10
	"Aakha #0" = -10
	"Sabana #0" = -10
	"Saba #0" = -10
	"Chena #0" = -10
	"Resa #0" = -10
	"Tuma #0" = -10
	"Taure #0" = -10
	"Panya #0" = -10
	"Arna #0" = -10
	"Raziya #0" = -10
	"Toriya #0" = -10
	"Subira #0" = -10
	"Taia #0" = -10
	"Ahti #0" = -10
	"Keta #0" = -10
}

leader_names = {
	#Country Specific
	Catchosen "'the Wanderer'" "'the Scout'" "'the Swift'"
	
	#Province Neighbours
	"of Aakheta" "of Sorrowgates" "of Anarat" "of Nirat" "of Kheterat" "of Koroshesh" 
	
	#Country Neighbours
	"of Brasan" "of Kheterata" "of Elizna" "of Bulwar"
	
	#Geographic Neighbours
	"of the Salahad" "of the Sorrow" "of the Diven" "of Akan"
	
	#Noble Families

	
	#Titles
	"'the Brown'"
	"'the Tall'" "'the Younger'" "'the Clever'" "'the Vigilant'" "'the Stern'" "'the Mighty'" "'Halfblood'" "'the Mudblooded'" "'the Kind'" "'the Able'" "'the Good'" "'the Fat'"
}

ship_names = {
	Elikhet "The Wanderer" "The Provider" Mother Sorrow "Mother's Sorrow" River
}

army_names = {
	"New Wanderers Army" "Wanderer" "Army of $PROVINCE$"
}

fleet_names = {
	"New Wanderers Fleet" "Cat Squadron" "Paw Squadron" "Fur Squadron" 
}