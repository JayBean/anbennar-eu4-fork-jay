# No previous file for Lipan

owner = B21
controller = B21
add_core = B21
culture = green_orc
religion = great_dookan
capital = ""

hre = no

base_tax = 2
base_production = 1
base_manpower = 1

trade_goods = unknown

native_size = 32
native_ferocity = 3
native_hostileness = 10