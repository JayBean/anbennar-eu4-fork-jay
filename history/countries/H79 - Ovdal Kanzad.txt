government = monarchy
add_government_reform = dwarven_clan_reform
government_rank = 1
primary_culture = amber_dwarf
religion = ancestor_worship
technology_group = tech_dwarven
capital = 2914

1000.1.1 = { 
	set_country_flag = mage_organization_centralized_flag
	add_country_modifier = {
		name = remnant_legacy
		duration = -1
	}
	add_country_modifier = {
		name = dwarven_administration
		duration = -1
	}
}