government = theocracy
add_government_reform = adventurer_reform
government_rank = 1
primary_culture = east_damerian
religion = regent_court
technology_group = tech_cannorian
capital = 765

1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }

1443.11.4 = {
	monarch = {
		name = "Rogier"
		dynasty = "Silmuna"
		birth_date = 1406.1.1
		adm = 3
		dip = 1
		mil = 5
	}
}
