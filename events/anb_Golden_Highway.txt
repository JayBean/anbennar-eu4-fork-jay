
namespace = golden_highway

#advance_a_stage
province_event = {
	id = golden_highway.1
	title = golden_highway.1.t
	desc = golden_highway.1.d
	picture = FARMING_eventPicture
	
	trigger = {
		OR = {
			has_province_modifier = golden_highway_under_construction
		}
	}
	mean_time_to_happen = {
		days = 1000
		modifier = {
			factor = 0.75
			full_idea_group = economic_ideas
		}
		modifier = {
			factor = 0.8
			full_idea_group = administrative_ideas
		}
		modifier = {
			factor = 0.9
			development = 10
		}
		modifier = {
			factor = 0.9
			development = 15
		}
		modifier = {
			factor = 0.8
			development = 20
		}
		modifier = {
			factor = 0.7
			development = 30
		}
		modifier = {
			factor = 0.7
			is_prosperous = yes
		}
		modifier = {
			factor = 2
			devastation = 1
		}
	}

	immediate = {
		hidden_effect = {
			if = {
				limit = { has_province_modifier = golden_highway_stage_1 }
				
				remove_province_modifier = golden_highway_stage_1
				add_permanent_province_modifier = {
					name = golden_highway_stage_2
					duration = -1
				}
			}
			else_if = {
				limit = { has_province_modifier = golden_highway_stage_2 }
				
				remove_province_modifier = golden_highway_under_construction
				remove_province_modifier = golden_highway_stage_2
				add_permanent_province_modifier = {
					name = golden_highway
					duration = -1
				}
			}
			else = {
				add_permanent_province_modifier = {
					name = golden_highway_stage_1
					duration = -1
				}
			}
		}
	}

	#Continue building
	option = {
		name = golden_highway.1.a
		
		trigger = { NOT = { has_province_modifier = golden_highway } }
		
		owner = { add_treasury = -50 }
	}
	
	#We don't have the funds for now
	option = {
		name = golden_highway.1.b
		
		trigger = { NOT = { has_province_modifier = golden_highway } }
				
		remove_province_modifier = golden_highway_under_construction
		add_permanent_province_modifier = {
			name = golden_highway_construction_stalled
			duration = -1
		}
		
		owner = { add_prestige = -5 }
	}
	
	#The road restored!
	option = {
		name = golden_highway.1.c
		
		trigger = { has_province_modifier = golden_highway }
		
		owner = { add_prestige = 5 }
	}
}

#resume construction
province_event = {
	id = golden_highway.2
	title = golden_highway.2.t
	desc = golden_highway.2.d
	picture = FARMING_eventPicture
	
	trigger = {
		OR = {
			has_province_modifier = golden_highway_construction_stalled
		}
	}
	mean_time_to_happen = {
		days = 3650
		modifier = {
			factor = 0.75
			full_idea_group = economic_ideas
		}
		modifier = {
			factor = 0.8
			full_idea_group = administrative_ideas
		}
		modifier = {
			factor = 0.9
			development = 10
		}
		modifier = {
			factor = 0.9
			development = 15
		}
		modifier = {
			factor = 0.8
			development = 20
		}
		modifier = {
			factor = 0.7
			development = 30
		}
		modifier = {
			factor = 0.7
			is_prosperous = yes
		}
		modifier = {
			factor = 2
			devastation = 1
		}
	}

	option = {
		name = golden_highway.2.a
		
		owner = { add_treasury = -30 }
				
		remove_province_modifier = golden_highway_construction_stalled
		add_permanent_province_modifier = {
			name = golden_highway_under_construction
			duration = -1
		}
	}

	#We don't have the funds for now
	option = {
		name = golden_highway.2.b
		
		owner = { add_prestige = -5 }
	}
}