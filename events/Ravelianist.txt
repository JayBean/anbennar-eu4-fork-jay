namespace = ravelianistist

#Disabled for now

# # Founding of the Ravelian Society
# country_event = {
	# id = ravelianist.1
	# title = ravelianist.1.t
	# desc = ravelianist.1.d
	# picture = RELIGION_eventPicture
	
	# major = yes
	
	# fire_only_once = yes
	
	# trigger = {
		# is_religion_enabled = corinite
		# religion_years = { corinite = 50 }
		# NOT = { is_religion_enabled = ravelian }
		# is_defender_of_faith = no
		# current_age = age_of_reformation
		# capital_scope = {
			# continent = europe
		# }
		# any_owned_province = {
			# can_have_center_of_reformation_trigger = {
					# RELIGION = ravelian
			# }
			# development = 20	#so its an intellectual thing!
		# }
	# }
	
	# mean_time_to_happen = {
		# months = 80
		
		# modifier = {
			# factor = 2
			# ruler_has_personality = free_thinker_personality
		# }
		# modifier = {
			# factor = 2
			# ruler_has_personality = righteous_personality
		# }
		# modifier = {
			# factor = 2
			# ruler_has_personality = scholar_personality
		# }
		
		# modifier = {
			# factor = 0.75
			# AND = {
				# NOT = { num_of_cities = 2 }	#city states
			# }
		# }
		
		# modifier = {
			# factor = 0.5
			# tag = A38
		# }
		# modifier = {
			# factor = 0.5
			# num_of_owned_provinces_with = {
				# value = 1
				# development = 30
			# }
		# }
		
		# modifier = {
			# factor = 0.5
			# religion_years = { corinite = 125 }
		# }
		# modifier = {
			# factor = 0.5
			# religion_years = { corinite = 150 }
		# }
		# modifier = {
			# factor = 0.9
			# innovativeness_ideas = 5
		# }
		# modifier = {
			# factor = 0.9
			# innovativeness_ideas = 6
		# }
		# modifier = {
			# factor = 0.9
			# innovativeness_ideas = 7
		# }
		# modifier = {
			# factor = 1.5
			# NOT = { innovativeness_ideas = 3 }
		# }
		# modifier = {
			# factor = 2
			# NOT = { innovativeness_ideas = 2 }
		# }
		# modifier = {
			# factor = 2
			# NOT = { innovativeness_ideas = 1 }
		# }
		# modifier = {
			# factor = 0.8
			# has_idea = humanist_tolerance
		# }	
		# modifier = {
			# factor = 0.5
			# num_of_rebel_controlled_provinces = 1
		# }
		# modifier = {
			# factor = 0.5
			# num_of_rebel_controlled_provinces = 1
		# }
	# }
	
	# option = {
		# name = "EVTOPTA2011"
		# enable_religion = ravelianist
		# random_owned_province = {
			# limit = {
				# can_have_center_of_reformation_trigger = {
					# RELIGION = ravelianist
				# }
				# development = 20	#so its an intellectual thing!
			# }
			# change_religion = ravelianist
			# add_reform_center = ravelianist
			# add_permanent_province_modifier = {
				# name = "religious_zeal_at_conv"
				# duration = 18000
			# }
			# if = {
				# limit = {
					# NOT = { is_capital = yes }
				# }
				# root = {
					# capital_scope = {
						# change_religion = ravelianist
						# add_permanent_province_modifier = {
							# name = "religious_zeal_at_conv"
							# duration = 18000
						# }
					# }
				# }
			# }
		# }
		# set_country_flag = "ravelianist_reformation" 
	# }
# }

# ravelianist Academic Circle in city

# ravelianist Academic Circle levels up into Society Chapter

# Paper 1

# Paper 2

# Paper 3

# Paper 4