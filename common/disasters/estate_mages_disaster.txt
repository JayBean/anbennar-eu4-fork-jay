estate_mages_disaster = {
	potential = {
		has_estate = estate_mages
		estate_influence = {
			estate = estate_mages
			influence = 80
		}
	}


	can_start = {
		has_any_disaster = no
		estate_influence = {
			estate = estate_mages
			influence = 100
		}
	}
	
	can_stop = {
		OR = {
			has_any_disaster = yes
			NOT = {
				estate_influence = {
					estate = estate_mages
					influence = 100
				}		
			}
		}
	}
	
	down_progress = {
		factor = 1
	}
	
	progress = {
		modifier = {
			factor = 5
			estate_influence = {
				estate = estate_mages
				influence = 100
			}
		}
	}
	
	can_end = {
		custom_trigger_tooltip = {
			tooltip = EST_CRUSHED_MAGES
			OR = {
				NOT = { has_country_flag = mages_estate_in_power }
				has_country_flag = noble_estate_in_power
				has_country_flag = church_estate_in_power
				has_country_flag = burghers_estate_in_power
			}						
		}
	}
	
	modifier = {	
		global_tax_modifier = -0.5
		prestige = -0.5
	}

	on_start = estate_disasters.15
	on_end = estate_disasters.16
	
	on_monthly = {
	}
}

