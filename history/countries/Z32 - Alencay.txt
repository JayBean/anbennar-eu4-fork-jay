government = monarchy
add_government_reform = feudalism_reform
government_rank = 1
primary_culture = blue_reachman
religion = regent_court
technology_group = tech_cannorian
capital = 706
national_focus = DIP

1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }

1440.4.11 = {
	monarch = {
		name = "Lan I"
		dynasty = "Alencay"
		birth_date = 1437.11.4
		adm = 0
		dip = 3
		mil = 2
	}
}