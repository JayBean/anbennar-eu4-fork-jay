government = monarchy
add_government_reform = dwarven_clan_reform
government_rank = 1
primary_culture = ruby_dwarf
religion = ancestor_worship
technology_group = tech_dwarven
national_focus = DIP
capital = 62
historical_friend = A01 #Lorent

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1404.7.23 = {
	monarch = {
		name = "Gronmar"
		dynasty = "Redstone"
		birth_date = 1352.8.8
		adm = 3
		dip = 3
		mil = 4
	}
}

1422.1.1 = { set_country_flag = lilac_wars_rose_party }