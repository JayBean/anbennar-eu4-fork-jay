government = theocracy
add_government_reform = adventurer_reform
government_rank = 1
primary_culture = ruby_dwarf
religion = ancestor_worship
technology_group = tech_dwarven
capital = 4056
fixed_capital = 4056

1404.23.7 = {
	monarch = {
		name = "Gramli"
		dynasty = "Redstone"
		birth_date = 1352.8.8
		adm = 4
		dip = 4
		mil = 4
	}
	add_ruler_personality = immortal_personality
	add_historical_friend = A20
}

1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }